﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PruebaGrupoExito.Application;
using PruebaGrupoExito.ConsoleApp;
using PruebaGrupoExito.Infrastructure;

namespace PruebaGrupoExito;
class Program
{
    static async Task Main(string[] args)
    {

        var builder = new ConfigurationBuilder();
        builder.SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

        var config = builder.Build();

        var serviceCollection = new ServiceCollection()
            .AddApplicationServices()
            .AddInfrastructureServices(config)
            .BuildServiceProvider();

        string option =string.Empty;
        
        do
        {
            Console.WriteLine("Prueba tecnica Grupo Exito \n" +
            "Selexione una de las siguientes opciones:\n" +
            "\t1 - Lista de movimientos.\n" +
            "\t2 - Listar 10 pokemons\n" +
            "\t3 - Salir");

            option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    await ConsultMoves(serviceCollection);
                    break;
                case "2":
                default:
                    break;
            }

        } while (option != "3");

    }

    private static async Task ConsultMoves(ServiceProvider serviceCollection)
    {

        var test = new MoveTest(serviceCollection.GetService<IMediator>());

        var moves = await test.RecursiveConsultMovement(0, 20, "move");

        moves.ForEach(m => Console.WriteLine(m.Name));
    }


}

