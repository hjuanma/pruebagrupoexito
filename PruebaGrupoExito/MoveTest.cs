﻿using System;
using System.Collections.Generic;
using System.Net;
using MediatR;
using PruebaGrupoExito.Application.Features;
using PruebaGrupoExito.Domain.Models.Movement;

namespace PruebaGrupoExito.ConsoleApp
{
	public class MoveTest
	{
		private readonly IMediator _mediator;

		public MoveTest(IMediator mediator)
		{
			_mediator = mediator;
		}

		public async Task GetAllMoves(int offset, int limit, string endpoint)
		{
			var query = new GetMovement()
			{
				EndPoint = endpoint,
                offset = offset,
				ConsultLimit = limit
			};

			var response = await _mediator.Send(query);
		}

		public async Task<List<Move>> RecursiveConsultMovement(int initiaOffset, int limit, string endpoint, int consulted = 0)
		{
			var result = new List<Move>();
			int taskNum = 10;
			if (consulted == 920)
			{
				return result;
			}
			else if ((920 - consulted) < 10)
			{
				taskNum = 920 - consulted;
            }

            Task<ApiResponseVM>[] taskArray = new Task<ApiResponseVM>[taskNum];

            var query = new GetMovement()
            {
                EndPoint = endpoint,
                offset = initiaOffset,
                ConsultLimit = limit
            };

            for (int i = 0; i < taskArray.Length; i++)
            {
                taskArray[i] = _mediator.Send(query);
				query.offset += 20;
            }

            ICollection<ApiResponseVM> apiResponses = await Task.WhenAll(taskArray);

            apiResponses.ToList().ForEach(r => result.AddRange(r.Results!));

			var concat = await RecursiveConsultMovement(query.offset, query.ConsultLimit, query.EndPoint, consulted + result.Count);
			result.AddRange(concat);

            return result;
		}
	}
}

