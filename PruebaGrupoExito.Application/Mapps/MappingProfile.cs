﻿using System;
using AutoMapper;
using PruebaGrupoExito.Application.Features;
using PruebaGrupoExito.Domain.Models.Movement;

namespace PruebaGrupoExito.Application.Mapps
{
	public class MappingProfile : Profile
    {
        public MappingProfile()
		{
            CreateMap<MovementResponse, ApiResponseVM>()
                .ForMember(dest => dest.Count, src => src.MapFrom(s => s.Count))
                .ForMember(dest => dest.Next, src => src.MapFrom(s => s.Next))
                .ForMember(dest => dest.Previous, src => src.MapFrom(s => s.Previous))
                .ForMember(dest => dest.Results, src => src.MapFrom(s => s.Results))
                .ReverseMap();
        }
	}
}

