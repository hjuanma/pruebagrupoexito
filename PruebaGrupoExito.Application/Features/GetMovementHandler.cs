﻿using System;
using AutoMapper;
using MediatR;
using PruebaGrupoExito.Application.Contracts;

namespace PruebaGrupoExito.Application.Features
{
	public class GetMovementHandler :  IRequestHandler<GetMovement, ApiResponseVM>
    {
        private readonly IApiResponseRepository _responseRepository;
        private readonly IMapper _mapper;

        public GetMovementHandler(IApiResponseRepository moveRepository, IMapper mapper)
        {
            _responseRepository = moveRepository;
            _mapper = mapper;
        }

        public async Task<ApiResponseVM> Handle(GetMovement request, CancellationToken cancellationToken)
        {
            var result = await _responseRepository.GetAsync(offset: request.offset, consultLimit: request.ConsultLimit, endpiont: request.EndPoint);
            return _mapper.Map<ApiResponseVM>(result);

        }
    }
}

