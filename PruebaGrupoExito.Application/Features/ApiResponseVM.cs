﻿using System;
using PruebaGrupoExito.Domain.Models.Movement;

namespace PruebaGrupoExito.Application.Features
{
	public class ApiResponseVM
    {
        #region Properties

        public int? Count { get; set; }
        public string? Next { get; set; }
        public string? Previous { get; set; }
        public List<Move>? Results { get; set; }

        #endregion
    }
}

