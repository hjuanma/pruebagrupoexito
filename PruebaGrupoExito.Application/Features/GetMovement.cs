﻿using System;
using MediatR;

namespace PruebaGrupoExito.Application.Features
{
	public class GetMovement : IRequest<ApiResponseVM>
	{
        #region Properties

        public int offset { get; set; }

        public int ConsultLimit { get; set; }

        public string? EndPoint { get; set; }
        #endregion
    }
}

