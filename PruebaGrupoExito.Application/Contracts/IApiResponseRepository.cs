﻿using System;
using PruebaGrupoExito.Domain.Models.Movement;

namespace PruebaGrupoExito.Application.Contracts
{
	public interface IApiResponseRepository : IAsyncRepository<MovementResponse>
	{
	}
}

