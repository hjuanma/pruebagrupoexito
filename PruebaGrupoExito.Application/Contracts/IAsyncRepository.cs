﻿using System;
using System.Linq.Expressions;

namespace PruebaGrupoExito.Application.Contracts
{
	public interface IAsyncRepository<T> where T : class
	{
		Task<T> GetAsync(int offset = 0, int consultLimit = 20, string endpiont = "", Expression<Func<T, bool>> expression = null);
	}
}

