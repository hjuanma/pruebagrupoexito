﻿using System;
namespace PruebaGrupoExito.Domain.Models.Movement
{
	public class Move
	{
        #region Properties
        public string? Name { get; set; }
        public string? Url { get; set; }
        #endregion
    }
}

