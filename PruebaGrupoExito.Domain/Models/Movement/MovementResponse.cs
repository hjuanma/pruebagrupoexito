﻿using System;
namespace PruebaGrupoExito.Domain.Models.Movement
{
	public class MovementResponse
    {
        #region Properties

        public int? Count { get; set; }
        public string? Next { get; set; }
        public string? Previous { get; set; }
        public List<Move>? Results { get; set; }

        #endregion
    }
}

