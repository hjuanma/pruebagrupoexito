﻿using System;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PruebaGrupoExito.Application;
using PruebaGrupoExito.Application.Contracts;
using PruebaGrupoExito.Infrastructure.Clients;
using PruebaGrupoExito.Infrastructure.Contracts;
using PruebaGrupoExito.Infrastructure.Models;
using PruebaGrupoExito.Infrastructure.Repositories;

namespace PruebaGrupoExito.Infrastructure
{
	public static class InfrastructureServiceRegistration
	{
		public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(IApiResponseRepository), typeof(ApiResponseRepository));
            services.AddHttpClient();
            services.AddScoped(typeof(IApiClient<>), typeof(BaseHttpClient<>));
            
            services.Configure<ApiSettings>(c => configuration.GetSection("ApiSettings"));
            return services;
        }
    }
}

