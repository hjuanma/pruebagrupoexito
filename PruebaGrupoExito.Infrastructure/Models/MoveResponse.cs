﻿using System;
using Newtonsoft.Json;

namespace PruebaGrupoExito.Infrastructure.Models
{
	public class MoveResponse
    {
        #region Properties
        [JsonProperty("name")]
        public string? Name { get; set; }
        [JsonProperty("url")]
        public string? Url { get; set; }
        #endregion
    }
}

