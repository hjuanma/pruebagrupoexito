﻿using System;
using Newtonsoft.Json;
using PruebaGrupoExito.Domain.Models.Movement;

namespace PruebaGrupoExito.Infrastructure.Models
{
	public class ApiResponse
    {
        #region Properties
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("next")]
        public string? Next { get; set; }

        [JsonProperty("previous")]
        public string? Previous { get; set; }

        [JsonProperty("results")]
        public List<MoveResponse>? Results { get; set; }
        #endregion
    }
}

