﻿using System;
using System.Linq.Expressions;
using PruebaGrupoExito.Application.Contracts;
using PruebaGrupoExito.Domain.Models.Movement;
using PruebaGrupoExito.Infrastructure.Contracts;
using PruebaGrupoExito.Infrastructure.Models;

namespace PruebaGrupoExito.Infrastructure.Repositories
{
    public class ApiResponseRepository : BaseRepository<MovementResponse>, IApiResponseRepository
    {
        public ApiResponseRepository(IApiClient<MovementResponse> api) : base(api)
        {
        }
    }
}

