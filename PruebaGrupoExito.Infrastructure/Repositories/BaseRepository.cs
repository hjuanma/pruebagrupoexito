﻿using System;
using System.Linq.Expressions;
using PruebaGrupoExito.Application.Contracts;
using PruebaGrupoExito.Infrastructure.Contracts;

namespace PruebaGrupoExito.Infrastructure.Repositories
{
	public class BaseRepository<T> : IAsyncRepository<T> where T : class
    {
        private readonly IApiClient<T> _apiClient;

		public BaseRepository(IApiClient<T> api)
		{
            _apiClient = api;
		}

        public async Task<T> GetAsync(int offset = 0, int consultLimit = 20, string endpiont = "", Expression<Func<T, bool>> expression = null)
        {
            return await _apiClient.GetValuesAsync(offset, consultLimit, endpiont);
        }
    }
}

