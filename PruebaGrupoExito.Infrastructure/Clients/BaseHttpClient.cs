﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.Extensions.Options;
using PruebaGrupoExito.Infrastructure.Contracts;
using PruebaGrupoExito.Infrastructure.Models;

namespace PruebaGrupoExito.Infrastructure.Clients
{
	public class BaseHttpClient<T> : IApiClient<T> where T : class
	{

		private readonly IHttpClientFactory _httpClientFactory;
		private readonly HttpClient _httpClient;
		private readonly ApiSettings _url;

		public BaseHttpClient(IHttpClientFactory httpClientFactory, IOptions<ApiSettings> url)
		{
			_httpClientFactory = httpClientFactory;
			_httpClient = _httpClientFactory.CreateClient();
			_httpClient.BaseAddress = new Uri("https://pokeapi.co/api/v2/");
		}

		public async Task<T> GetValuesAsync(int offset = 0, int limitConsult = 20, string endpoint = "")
		{
			//using (var response = await _httpClient.GetAsync($"{_url.UrlBaseString}{endpoint}/?limit={limitConsult}&offset=20"))
            using (var response = await _httpClient.GetAsync($"{endpoint}/?limi={limitConsult}&offset={offset}"))
            {
				if (response.IsSuccessStatusCode)
				{
					return await response.Content.ReadFromJsonAsync<T>();
				}
				return null;
			}
		}
	}
}

