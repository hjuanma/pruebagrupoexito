﻿using System;
using System.Net;

namespace PruebaGrupoExito.Infrastructure.Contracts
{
	public interface IApiClient<T>
	{
        Task<T> GetValuesAsync(int offset, int limitConsult, string EndPoint);
    }
}

